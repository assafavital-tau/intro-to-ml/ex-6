import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import numpy

def plot(filename, xarr, yarr,
         xscale='linear', yscale='linear', title=None, xlabel=None, ylabel=None, legends=None, grid=True, points_of_interest=None):
    '''
    Create a plot from matplotlib
    :param filename: Name of output .PNG file
    :param xarr: Array corresponding to x values
    :param yarr: Array corresponding to y values
    :param xscale: Scale of x axis (default = 'linear')
    :param yscale: Scale of y axis (default = 'linear')
    :param title: Title of plot (optional)
    :param xlabel: Label of x axis (optional)
    :param ylabel: Label of y axis (optional)
    :param grid: Visibility of grid (shown by default)
    :param points_of_interest: Mark points of interest above the said plot (optional)
    :return: 
    '''

    if legends is None:
        legends = [None for y in yarr]
    fig = plt.figure()
    for i in range(len(yarr)):
        plt.plot(xarr, yarr[i], label=legends[i])
    plt.xscale(xscale)
    if(title != None):
        plt.title(title)
    if(xlabel != None):
        plt.xlabel(xlabel)
    if(ylabel != None):
        plt.ylabel(ylabel)
    if(grid):
        plt.grid()
    if(points_of_interest != None):
        for p in points_of_interest:
            plt.plot(p[0], p[1], '*')
            plt.annotate("({0},{1})".format(p[0], p[1]), xy=(p[0], p[1]))
    plt.legend()
    fig.savefig(filename)
    plt.close()