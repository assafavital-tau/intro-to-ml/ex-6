Submitters information:
-
    NAME            I.D.        E-MAIL                      USERNAME
    Assaf Avital    204097588   assafavital9@gmail.com      assafavital
    Yahav Avigal    200921740   yahavigal@gmail.com         yahavavigal

Code path:

    /specific/a/home/cc/students/cs/assafavital/ml6
Plots path:

    /specific/a/home/cc/students/cs/assafavital/ml6/Q<subsection>.png
---
SGD:
-
COMMAND-LINE ARGUMENTS:

    "1a"    :   Execute subsection 1.A
    "1b"    :   Execute subsection 1.B
    "1c"    :   Execute subsection 1.C
    "1d"    :   Execute subsection 1.D

Running subsection 1.B for example:

    python sgd.py 1b
    

Back Propagation:
-
COMMAND-LINE ARGUMENTS:

    "2a"    :   Execute subsection 2.A
    "2b"    :   Execute subsection 2.B
    "2c"    :   Execute subsection 2.C
    "2d"    :   Execute subsection 2.D

Running subsection 2.C for example:

    python bp.py 2c
    
TensorFlow:
-
Our modified verson of _tf_mnist.py_ exists in the given path.
COMMAND_LINE ARGUMENTS:

    "3a"    :   Execute subsection 3.A
    "3b"    :   Execute subsection 3.B
    "3c"    :   Execute subsection 3.C
    "3d"    :   Execute subsection 3.D
    "3e"    :   Execute subsection 3.E
    
Running subsection 3.A for example:

    python tf_mnist.py 3a